import {url} from '../../app.json';
import {NativeModules, Platform} from 'react-native';
import Geolocation from '@react-native-community/geolocation';

const getDeviceLocale = () => {
    const deviceLanguage =
          Platform.OS === 'ios'
            ? NativeModules.SettingsManager.settings.AppleLocale ||
              NativeModules.SettingsManager.settings.AppleLanguages[0]
            : NativeModules.I18nManager.localeIdentifier;
    return deviceLanguage.split('_')[0];
}

const getCurrentPosition = function (options) {
    return new Promise((resolve, reject) => {
        Geolocation.getCurrentPosition(resolve, reject, options);
    });
}

const getCoordinates = async () => {
    const position = await getCurrentPosition();
    const {coords: {longitude, latitude}} = position;
    return [longitude, latitude];
}

const postRequest = async (url, body) => {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    });
    const responseBody = await response.json();
    return responseBody;
}

const getRequest = async (url, body) => {
    const response = await fetch(url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    });
    const responseBody = await response.json();
    return responseBody;
}

const initialText = async () => {
    const locale = getDeviceLocale();
    const body = {language: locale};
    const response = await postRequest(`${url}/app/`, body);
    return response;
}

const talk = async message => {
    const locale = getDeviceLocale();
    const body = {text: message, language: locale};
    const response = await postRequest(`${url}/app/talk`, body);
    return response;
}

const closePlaces = async () => {
    const [lng, lat] = await getCoordinates();
    const response = await getRequest(`${url}/app/near/${lng}/${lat}`);
    return response;
}

const bestPlace = async () => {
    const [lng, lat] = await getCoordinates();
    const response = await getRequest(`${url}/app/best/${lng}/${lat}`);
    return response;
}

module.exports = {
    bestPlace,
    closePlaces,
    initialText,
    talk,
};

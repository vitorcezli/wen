import React from 'react';
import {Header} from 'react-native-elements';
import {appColor, statusBarColor} from '../assets/colors/colors.json';

const CallHeader = (props) => (
  <Header
    placement="center"
    backgroundColor={appColor}
    statusBarProps={{backgroundColor: statusBarColor}}
    centerComponent={{
      text: 'Wen',
      style: {color: 'white', fontWeight: 'bold'},
    }}
  />
);

export default CallHeader;

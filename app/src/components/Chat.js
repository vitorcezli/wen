import React from 'react';
import {Bubble, GiftedChat, Send} from 'react-native-gifted-chat';
import {Image, StyleSheet} from 'react-native';
import connection from '../utils/connection';

const wenUser = {
  _id: 2,
  name: 'Wen',
  avatar: require('../assets/images/chat.png'),
};

const user = {
  _id: 1,
};

export default class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
    };
    this.appendUserMessage = this.appendUserMessage.bind(this);
    this.appendWenMessage = this.appendWenMessage.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.processBestPlace = this.processBestPlace.bind(this);
    this.processClosePlaces = this.processClosePlaces.bind(this);
    connection.initialText().then(response => {
      this.appendWenMessage(response.text);
    });
  }

  processBestPlace() {
    connection.bestPlace().then(place => {
      const responseWen = `${place.name}\n\n${place.address}`
      this.appendWenMessage(responseWen);
    });
  }

  processClosePlaces() {
    connection.closePlaces().then(places => {
      places.forEach(place => {
        const text = `${place.name} (${(100 * place.occupied / place.capacity).toFixed(1)}%)`;
        this.appendWenMessage(text);
      });
    });
  }

  sendMessage(message) {
    connection.talk(message).then(response => {
      this.appendWenMessage(response.text);
      if (response.hospitalInfo) {
        this.processClosePlaces();  
      } else if (response.service) {
        this.processBestPlace();     
      }
    });
  }

  appendUserMessage(message) {
    const messages = GiftedChat.append(this.state.messages, message);
    this.setState({messages: messages});
    this.sendMessage(message[0].text);
  }

  appendWenMessage(message) {
    const id = this.state.messages.length + 1;
    const messageBody = {
      _id: id,
      createdAt: new Date(),
      text: message,
      user: wenUser
    }
    const messages = GiftedChat.append(this.state.messages, messageBody);
    this.setState({messages: messages});
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        textStyle={{
          right: {
            color: 'white',
          },
        }}
        wrapperStyle={{
          right: {
            backgroundColor: 'gray',
          },
        }}
      />
    );
  }

  renderSend(props) {
    return (
      <Send {...props} containerStyle={styles.sendContainer}>
        <Image
          source={require('../assets/images/send.png')}
          style={styles.image}
        />
      </Send>
    );
  }

  render() {
    return (
      <GiftedChat
        placeholder=""
        messages={this.state.messages}
        renderBubble={this.renderBubble}
        renderSend={this.renderSend}
        user={user}
        onSend={(messages) => this.appendUserMessage(messages)}
      />
    );
  }
}

const styles = StyleSheet.create({
  sendContainer: {
    height: 42,
    width: 42,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 30,
    height: 30,
  },
});

import React from 'react';

import {View} from 'react-native';

import CallHeader from './src/components/CallHeader';
import Chat from './src/components/Chat';

const App: () => React$Node = () => {
  return (
    <View style={{flex: 1}}>
      <CallHeader />
      <Chat />
    </View>
  );
};

export default App;

This folder contains the mobile application code of Wen. Refer to the following
steps to configure and run this application.

## Configuration:

1. Modify **url** field on *app.json* file with the URL of your Wen's server.
2. Configure your environment to run React Native applications (https://reactnative.dev/docs/0.61/getting-started)
3. Install the necessary packages by running `npm install`.

## Running:

1. Start React Native: `react-native start`
2. Run the application: `npx react-native run-android` or `npx react-native run-ios`

## Troubleshooting:

Depending on your version of React Native, some errors will occur when you try
to run the application. You will need to create some links, by executing
`react-native link @react-native-community/geolocation` and
`react-native link react-native-vector-icons`.

As the authors don't have access to the necessary hardware, the command
`npx react-native run-ios` was not tested. This way, some errors may occur when
executing Wen on iOS platform, but the authors believe that these errors are
easy to troubleshoot.
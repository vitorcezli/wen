## Configuration:

Create an instance of Watson Assistant and import *skill-covid.json* on its skill.

## References:

Wen's assistant uses the following references on its response about COVID:

- Google Search (symptoms of COVID-19)
- World Health Organization (what COVID-19 is)

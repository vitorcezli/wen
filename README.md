## Why Wen?

A tribute to Li **Wen**liang, the doctor that alerted the world about COVID-19.


## Short description:

Solution to find nearby hospitals with available hospital beds.


## Demo video:

A demonstration video is available on this [link](https://youtu.be/TxdaWX9yJ_Y).


## Long description:

By browsing the apps about COVID-19 on the Play Store and the Apple Store, you
can find those who monitor cases of this disease worldwide, providing figures on
how many people are infected and how many deaths have occurred. You can also find applications that report the symptoms of COVID-19 and others that indicate what
actions you should take to prevent it.

However, there is a common difficulty in many countries with no available
solution: finding available hospital beds during the COVID-19 pandemic. In India,
patients need to travel to find hospitals with free beds because there is no
system to track the availability of hospital beds in real-time, having many
people dying because of it. In Canada, some insurers make phone calls to
individual hospitals in the region where the patient lives. And in Manaus, a
city of Brazil isolated by geography and overwhelmed by disease, some patients
are dying after hunting for hours hospitals with available beds.

Wen is created as a solution to this problem, providing people with information
about the availability of beds in nearby hospitals. It consists of a website and
an application. The former is used by hospital administrators to add information
about hospitals and the number of beds occupied. Using this information and
indicating their GPS location, people use the latter to chat with Wen and retrieve 
real-time information about the occupation of the hospitals near them. The people
can also ask Wen about COVID-19, receiving a text from the World Health
Organization, informing about this disease and its symptoms. With this
information, people can tell the application if they think they are infected,
and Wen will recommend a nearby hospital with available beds to care for them.

As Wen solves a world problem, its engineers adopted technologies that process
geolocation anywhere in the world and that can talk with its users despite their
language, making the solution accessible to everyone.


## Roadmap:

![alt text](docs/roadmap.png "Roadmap")


## Services used by Wen:

### IBM Cloud:

- **Watson Assistant**: Used to send the messages of clients to Watson
to extract their intent. Based on intents returned on response, the corresponding
client will make a request to the server to retrieve the information of the
hospitals close to him/her.
- **Watson Translator**: The focus is to make Wen available for the
users around the world. This way, Watson Translator is used to translate the
clients' messages to English before passing them to Watson Assistant, where their
intents will be extracted. When Watson Assistant answers, its text is also
translated to the source language using Watson Translator.
- **Cloud Foundry**: The PaaS service where Wen's server is hosted.

### External services:

- **MapQuest**: Its geocoding is used to translate addresses of hospitals to GPS
coordinates. Geospatial query is performed on MongoDB to retrieve the hospitals
that are close to the clients based on these coordinates.
- **MongoDB Atlas**: Use MongoDB to store documents with information of hospitals.
MongoDB is also serviced on IBM Cloud, but MongoDB Atlas was used because it has
a free tier.


## Installation:

Each subfolder (*app*, *assistant* and *server*) contains part of Wen. Refer to
their README files to install the corresponding part.


## Authors:

- **Vítor Cézar de Lima** (vitorcezli@gmail.com)
- **Ricardo Barbosa Filho** (ricardobsaf@gmail.com)
- **Virna Gabriela Alves Machado** (virnagabriela05@gmail.com)


## License:

This project is licensed under the Apache 2 License - see the *LICENSE* file
for details.

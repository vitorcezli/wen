const mongoose = require('mongoose');

const pointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true,
    },
    coordinates: {
        type: [Number],
        required: true,
    },
});

const placeSchema = new mongoose.Schema({
    location: {
        type: pointSchema,
        index: '2dsphere',
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    capacity: {  // Total number of hospital beds.
        type: Number,
        required: true,
    },
    occupied: {  // Number of occupied hospital beds.
        type: Number,
        default: 0,
        validate: function (v) {
            return v >= 0 && v <= this.capacity;
        }
    }
});

const placesModel = mongoose.model('places', placeSchema);
module.exports = placesModel;

const LanguageTranslatorV3 = require('ibm-watson/language-translator/v3');
const {IamAuthenticator} = require('ibm-watson/auth');
const {translatorApi: API, translatorUrl: URL} = require('../config');

const languageTranslator = new LanguageTranslatorV3({
    version: '2018-05-01',
    authenticator: new IamAuthenticator({apikey: API}),
    url: URL,
});

const translate = async params => {
    const translated = await languageTranslator.translate(params);
    const {
        result: {
            translations: [{
                translation
            }]
        }
    } = translated;
    return translation;
}

const translateToEnglish = async (text, sourceLanguage) => {
    if (sourceLanguage === 'en') {
        return text;
    }
    const params = {
        text: text,
        modelId: `${sourceLanguage}-en`,
    };
    return translate(params);
}

const translateFromEnglish = async (text, targetLanguage) => {
    if (targetLanguage === 'en') {
        return text;
    }
    const params = {
        text: text,
        modelId: `en-${targetLanguage}`,
    };
    return translate(params);
}

module.exports = {
    translateFromEnglish,
    translateToEnglish,
};

This folder contains the code of Wen's server, which uses the following services:
**Watson Assistant (IBM Cloud)**, **MapQuest**, **MongoDB (MongoDB Atlas)**,
**Watson Translator (IBM Cloud)** and **Cloud Foundry (IBM Cloud)**.


## Before running the server...

Assert the following environment variables are defined:

- **ASSISTANT_API**: Available on Watson Assistant page.
- **ASSISTANT_URL** and **ASSISTANT_ID**, available on catalog after Watson
Assistant is created.
- **TRANSLATOR_API** and **TRANSLATOR_URL**, available on catalog after this
service is created.
- **MONGO_URL**: URI to connect to MongoDB (https://docs.mongodb.com/manual/reference/connection-string/).
- **MAPQUEST_KEY**: Key for MapQuest (https://developer.mapquest.com/documentation/).
- **PLACES_TOKEN**: It is currently used to secure requests to */places* route
and to authenticate on the website. When our solution start having multiuser
support, user sessions will be used instead.
- **DISTANCE_METERS**: The maximum distance on meters from the client the hospital
should be located to be considered "close". We are using 13 kilometers (13000)
on our solution.
- **LIMIT_PERIOD_TIME**: Time on milisseconds the client must wait for consecutive
messages to server. It is used to mitigate DDoS and it defaults to one second
(1000) on our solution.
- **PORT**: Port where the server will be hosted. It is automatically defined on
Cloud Foundry.


## Run the server:

Execute `npm install` to install the necessary packages, then execute `npm start`.


## Running using CI:

Wen's server is automatically deployed by using Gitlab-CI (configured on
*.gitlab-ci.yml* file). You can fork this repository and set the above environment
variables on your CI/CD configuration to run your own instance of Wen's server.

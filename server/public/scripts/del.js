function del(id) {
    var auth = document.cookie.split('=')[1];
    if (auth) {
        var url = document.location.origin;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState === 4) {
                if (xmlHttp.status === 200) {
                    window.location.href = '/';
                } else {
                    alert('Error updating beds');
                }
            }
        }
        xmlHttp.open('DELETE', url + '/places/' + id, true);
        xmlHttp.setRequestHeader('Authorization', auth);
        xmlHttp.send(null);
    }
}

$('#button-add').click(function () {
    var id = $('#form-add').serializeArray()[0]['value'];
    del(id);
});

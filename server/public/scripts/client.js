function appendIdRow(tr, id) {
    var th = document.createElement('th');
    th.setAttribute('scope', 'row');
    th.innerText = id;
    tr.appendChild(th);
}

function appendRow(tr, value) {
    var td = document.createElement('td');
    td.innerText = value;
    tr.appendChild(td);
}

function createElement(tbody, place) {
    var tr = document.createElement('tr');
    appendIdRow(tr, place._id);
    appendRow(tr, place.name);
    appendRow(tr, place.address);
    appendRow(tr, place.occupied);
    appendRow(tr, place.capacity);
    tbody.appendChild(tr);
}

function addPlacesOnTable(places) {
    var tbody = document.getElementById('places');
    for (var i = 0; i < places.length; i++) {
        createElement(tbody, places[i]);
    }
}

var auth = document.cookie.split('=')[1];
if (auth) {
    var url = document.location.origin;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var places = JSON.parse(xmlHttp.responseText);
            addPlacesOnTable(places);
        }
    }
    xmlHttp.open('GET', url + '/places', true);
    xmlHttp.setRequestHeader('Authorization', auth);
    xmlHttp.send(null);
}
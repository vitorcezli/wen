function postData(body) {
    var auth = document.cookie.split('=')[1];
    if (auth) {
        var url = document.location.origin;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState === 4) {
                if (xmlHttp.status === 200) {
                    window.location.href = '/';
                } else {
                    alert('Error sending data');
                }
            }
        }
        xmlHttp.open('POST', url + '/places', true);
        xmlHttp.setRequestHeader('Authorization', auth);
        xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xmlHttp.send(JSON.stringify(body));
    }
}

function getFormData() {
    var unindexed_array = $('#form-add').serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}

$('#button-add').click(function () {
    var body = getFormData();
    postData(body);
});

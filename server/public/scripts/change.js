function patch(values) {
    var auth = document.cookie.split('=')[1];
    if (auth) {
        var url = document.location.origin;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState === 4) {
                if (xmlHttp.status === 200) {
                    window.location.href = '/';
                } else {
                    alert('Error updating beds');
                }
            }
        }
        xmlHttp.open('PATCH', url + '/places/' + values[0] + '/' + values[1], true);
        xmlHttp.setRequestHeader('Authorization', auth);
        xmlHttp.send(null);
    }
}

function getValues() {
    var array = $('#form-add').serializeArray();
    var values = [array[0]['value'], array[1]['value']];
    return values;
}

$('#button-add').click(function () {
    var values = getValues();
    patch(values);
});

const AssistantV2 = require('ibm-watson/assistant/v2');
const {IamAuthenticator} = require('ibm-watson/auth');
const {
    assistantApi: API,
    assistantUrl: URL,
    assistantId: ID,
} = require('../config');

const assistant = new AssistantV2({
    version: '2020-04-01',
    authenticator: new IamAuthenticator({apikey: API}),
    url: URL,
});

const sendMessage = async message => {
    const response = await assistant.messageStateless({
        assistantId: ID,
        input: {
            'message_type': 'text',
            'text': message,
        }
    });
    const {
        output: {
            intents,
            generic,
        }
    } = response.result;
    return [intents.length ? intents[0].intent : null,
            generic.length ? generic[0].text : null];
}

module.exports = {
    sendMessage
};

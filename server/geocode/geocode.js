const {mapquestKey: KEY} = require('../config');
const axios = require('axios');

coordinates = async address => {
    // MapQuest isn't working with UTF-8. Maybe it just works with ASCII.
    const normAddr = address.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    const url = `https://open.mapquestapi.com/geocoding/v1/address?` +
                `key=${KEY}&location=${normAddr}`;
    try {
        const axiosResponse = await axios.get(url);
        const {
            data: {
                results: [{
                    locations: [{
                        latLng: {
                            lng,
                            lat,
                        }
                    }]
                }]
            }
        } = axiosResponse;
        return [lng, lat];
    } catch (err) {
        console.error(err);
        return;
    }
};

module.exports = coordinates;

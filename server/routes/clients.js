const express = require('express');
const app = express.Router();

app.get('/', (req, res) => {
    res.render("clients/client", {title: "Wen"});
});

app.get('/change', (req, res) => {
    res.render('clients/change', {title: 'Change Beds'});
});

app.get('/add', (req, res) => {
    res.render('clients/add', {title: 'Add Hospital'});
});

app.get('/del', (req, res) => {
    res.render('clients/del', {title: 'Delete Hospital'});
});

module.exports = app;

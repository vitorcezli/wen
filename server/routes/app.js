const {places: placesModel} = require('../models');
const {distanceMeters: METERS} = require('../config');
const {limitPeriodTime: PERIOD_TIME} = require('../config');
const assistant = require('../assistant');
const translator = require('../translator');

const express = require('express');
const app = express.Router();

// Limit the following routes to be used just once on five seconds.
// It mitigates DDoS attack.
const rateLimit = require("express-rate-limit");
const limiter = rateLimit({windowMs: PERIOD_TIME, max: 1});
app.use(limiter);

app.get('/near/:lng/:lat', async (req, res) => {
    try {
        const places = await placesModel.find({
            location: {
                $near: {
                    $maxDistance: METERS,
                    $geometry: {
                        type: "Point",
                        coordinates: [req.params.lng, req.params.lat]
                    }
                }
            }
        }).select('name address capacity occupied -_id');
        return res.json(places);
    } catch (err) {
        return res.sendStatus(400);
    }
});

app.get('/best/:lng/:lat', async (req, res) => {
    try {
        const places = await placesModel.find({
            location: {
                $near: {
                    $maxDistance: METERS,
                    $geometry: {
                        type: "Point",
                        coordinates: [req.params.lng, req.params.lat]
                    }
                }
            },
        }).select('name address capacity occupied -_id');
        // Assert there is a place close to the user.
        if (!places.length) {
            return res.json({});
        }
        // Get the place with more empty beds.
        const diff = element => element.capacity - element.occupied
        const bestPlace = places.reduce((last, current) => {
            return diff(last) > diff(current) ? last : current;
        });
        // Assert the place has empty beds.
        if (diff(bestPlace) > 0) {
            return res.json(bestPlace);
        }
        // There is no place with empty beds.
        return res.json({});
    } catch (err) {
        return res.sendStatus(400);
    }
});

app.post('/', async (req, res) => {
    try {
        let text = "I'm Wen. I can help you by giving information about COVID-19, " + 
            "showing nearby hospitals with available beds or recommending to " +
            "which hospital you should go if you think you are infected.";
        // Translate initial text to the desired language.
        const language = req.body.language;
        if (language) {
            text = await translator.translateFromEnglish(text, language);
        }
        // Return response to client.
        return res.json({text: text});
    } catch (err) {
        return res.sendStatus(500);
    }
});

app.post('/talk', async (req, res) => {
    try {
        // Translate the text to English.
        const language = req.body.language;
        const text = await translator.translateToEnglish(req.body.text, language);
        // Talk to assistant.
        const [intent, assistantText] = await assistant.sendMessage(text);
        // Configure flags and return text.
        let returnText, hospitalFlag = false, serviceFlag = false;
        switch (intent) {
            case 'hospital-info':
                hospitalFlag = true;
                returnText = 'Hospitals close to you (% of occupation)';
                break;
            case 'service':
                serviceFlag = true;
                returnText = 'You should go to this hospital';
                break;
            default:
                returnText = assistantText;
        }
        const translated = await translator.translateFromEnglish(returnText, language);
        // Return response to client.
        return res.json({
            hospitalInfo: hospitalFlag,
            service: serviceFlag,
            text: translated,
        });
    } catch (err) {
        return res.sendStatus(500);
    }
});

module.exports = app;

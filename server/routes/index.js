const app = require('./app');
const auth = require('./auth');
const clients = require('./clients');
const places = require('./places');

module.exports = {
    app,
    auth,
    clients,
    places,
};

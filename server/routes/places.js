const {placesToken: TOKEN} = require('../config');
const {places: placesModel} = require('../models');
const {coordinates} = require('../geocode');

const express = require('express');
const app = express.Router();

// Add middleware to authenticate the user.
app.use((req, res, next) => {
    if (req.headers.authorization === TOKEN) {
        next();
    } else {
        return res.sendStatus(401);
    }
});

app.post('/', async (req, res) => {
    const [lng, lat] = await coordinates(req.body.address);
    req.body.location = {
        type: 'Point',
        coordinates: [lng, lat],
    };
    const places = new placesModel(req.body);
    try {
        await places.save();
        return res.sendStatus(200);
    } catch (err) {
        if (err.code === 16755) {
            return res.sendStatus(400);
        }
        return res.sendStatus(500);
    }
});

app.get('/', async (req, res) => {
    try {
        const places = await placesModel.find();
        return res.json(places);
    } catch (err) {
        return res.sendStatus(500);
    }
});

app.get('/:id', async (req, res) => {
    try {
        const place = await placesModel.findById(req.params.id);
        return res.json(place);
    } catch (err) {
        return res.sendStatus(500);
    }
});

app.patch('/:id/:occupied', async (req, res) => {
    try {
        await placesModel.findByIdAndUpdate(
            {_id: req.params.id},
            {occupied: req.params.occupied},
        );
        return res.sendStatus(200);
    } catch (err) {
        return res.sendStatus(500);
    }
});

app.delete('/:id', async (req, res) => {
    try {
        await placesModel.deleteOne({_id: req.params.id});
        return res.sendStatus(200);
    } catch (err) {
        return res.sendStatus(500);
    }
});

module.exports = app;

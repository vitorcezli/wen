const {placesToken: TOKEN} = require('../config');
const express = require('express');
const app = express.Router();

app.get('/', (req, res) => {
    return res.render('auth/signin', {title: 'Wen'});
});

app.post('/', (req, res) => {
    if (req.body.token === TOKEN) {
        // Set cookie used for authentication on /places.
        res.cookie('auth', TOKEN);
        return res.redirect('/');
    }
    return res.render('auth/signin', {title: 'Wen', message: 'Invalid credentials'});
});

app.get('/logout', (req, res) => {
    res.clearCookie('auth');
    return res.redirect('/');
});

module.exports = app;

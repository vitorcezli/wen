// Retrieve configuration variables.
const {
    mongoUrl: MONGO_URL,
    port: PORT,
    placesToken: TOKEN
} = require('./config');

// Connect with MongoDB.
const mongoose = require('mongoose');
mongoose.connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
});

// Initialize express.
const express = require('express');
const app = express();

// Configure body-parser.
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Configure static public.
const path = require("path");
app.use(express.static(path.join(__dirname, 'public')));

// Configure favicon.
const favicon = require('serve-favicon');
app.use(favicon(path.join(__dirname, 'public', 'images', 'logo_zoom.ico')));

// Configure view engine.
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// Configure logger.
const logger = require("morgan");
app.use(logger("dev"));

// Configure cookie.
const cookieParser = require("cookie-parser");
app.use(cookieParser());

// Configure route for applications.
const {app: appRouter} = require('./routes');
app.use('/app', appRouter);

// Configure authentication.
const {auth: authRouter} = require('./routes');
app.use('/auth', authRouter);
app.use('*', (req, res, next) => {
    if (!req.cookies.auth || (req.cookies.auth != TOKEN)) {
        return res.redirect('/auth');
    }
    next();
});

// Configure routes.
const {
    places: placesRouter,
    clients: clientsRouter,
} = require('./routes');
app.use('/places', placesRouter);
app.use('/clients', clientsRouter);

// Configure to send home page to /clients route.
app.use('/', (req, res) => {
    res.redirect('/clients');
});

// Define NOT FOUND page.
app.use((req, res) => {
    res.status(404);
    return res.render('error/notfound');
});

// Run server.
const http = require('http');
const server = http.createServer(app);
server.listen(PORT);
